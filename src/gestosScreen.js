import React from "react";
import { View, PanResponder, StyleSheet,Text, Dimensions } from "react-native";


const Gesto = ({ navigation }) => {

    const screenWidth = Dimensions.get('window'). width;     // tamanho do equipamento ( janela )
    const panResponder = React.useRef (

        PanResponder.create({
            // Identifica o inicio do gesto 
            onStartShouldSetPanResponder: () => true,

            // Identifica a execução do gesto ( Movimento )
            onPanResponderMove: (event, gestureState) => {
                console.log('Movimento X:', gestureState.dx);    // captando movimentos 
                console.log('Movimento Y:', gestureState.dy);    // captando movimentos
            },

            // Identifica o final do gesto 
            onPanResponderRelease: (event, gestureState) => {
                if(gestureState.dx > screenWidth / 2) {

                    // Verifica se passou da metade da tela, navegação de telas 
                    navigation.goBack();
                } 
                            
            }
        })
    ).current;
    
    return(
        <View style={styles.container}
            {...panResponder.panHandlers}           // chamando todas as funções criadas nos blocos acima da linha 7 a 25.
        >
            <Text style={styles.text}>Arraste para a direita</Text>
        </View>
    );
};

const styles = StyleSheet.create({
    container:{
        flex: 1,
        alignItems:'center',   // alinhar tudo ao centro. 
        justifyContent:'center',
        backgroundColor:'#fff',
    },
    text:{
        fontSize:20,
        fontWeight:'bold',     // texto em negrito.
    }
})

export default Gesto;


// pan - mover, mexer, uma resposta a um gesto.  React - objeto, estou importando para acessar um objeto dentro dele 
                                                                // (useRef, useState, useEffect)